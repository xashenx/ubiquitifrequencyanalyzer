from __future__ import division
import sys
import re
import subprocess


def ping_device(address, ping_count):
    try:
        # ping = subprocess.Popen(["ping", "-c " + str(ping_count), address], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        ping = subprocess.Popen(["ping", "-c " + str(ping_count), "-i " + str(0.3), address], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, error = ping.communicate()
        result = {}
        if out:
            try:
                result = parse(out, 0)
            except:
                print("no data for one of minimum,maximum,average,packet")
        else:
            logging.error('No ping')

    except subprocess.CalledProcessError:
        print("Couldn't get a ping")
    return result

def fping_device(address, ping_count):
    try:
        ping = subprocess.Popen(["/usr/bin/fping", "-c " + str(ping_count), "-i " + str(10), address], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, error = ping.communicate()
        result = {}
        if error:
            try:
                result = parse(str(error), 1)
            except:
                print("no data for one of minimum,maximum,average,packet")
        else:
            logging.error('No ping')
    except subprocess.CalledProcessError:
        print("Couldn't get a ping")
    return result


def parse(ping_output, mode):
    """
    Parses the `ping_output` string into a dictionary containing the following
    fields:
    `host`: *string*; the target hostname that was pinged
    `sent`: *int*; the number of ping request packets sent
    `received`: *int*; the number of ping reply packets received
    `minping`: *float*; the minimum (fastest) round trip ping request/reply
    time in milliseconds
    `avgping`: *float*; the average round trip ping time in milliseconds
    `maxping`: *float*; the maximum (slowest) round trip ping time in
    milliseconds
    `jitter`: *float*; the standard deviation between round trip ping times
    in milliseconds
    """
    if not mode:
        matcher = re.compile(r'PING ([a-zA-Z0-9.\-]+) \(')
        host = _get_match_groups(ping_output, matcher)[0]
        matcher = re.compile(r'(\d+) packets transmitted, (\d+) received')
        sent, received = _get_match_groups(ping_output, matcher)
        try:
            loss = (float(sent)-float(received))/float(sent)*100
        except:
            print(sys.exc_info()[0])
            loss = 0

        try:
            matcher = re.compile(r'(\d+.\d+)/(\d+.\d+)/(\d+.\d+)/(\d+.\d+)')
            minping, avgping, maxping, jitter = _get_match_groups(ping_output,matcher)
        except:
            minping, avgping, maxping, jitter = ['NaN']*4
        return {'host': host, 'sent': sent, 'received': received,
            'minping': minping, 'avgping': avgping, 'maxping': maxping,
            'jitter': jitter, 'loss': loss}
    else:
        matcher = re.compile(r'([0-9.]+) \:')
        host = _get_match_groups(ping_output, matcher)[0]
        matcher = re.compile(r'xmt\/rcv\/%loss = (\d+)\/(\d+)\/(\d+)')
        sent, received, loss = _get_match_groups(ping_output, matcher)
        try:
            matcher = re.compile(r'min\/avg\/max = ([0-9.]+)\/([0-9.]+)\/([0-9.]+)')
            minping, avgping, maxping = _get_match_groups(ping_output,matcher)
        except:
            minping, avgping, maxping = ['NaN']*3
        return {'host': host, 'sent': sent, 'received': received,
            'minping': minping, 'avgping': avgping, 'maxping': maxping,
            'loss': float(loss)}

def _get_match_groups(ping_output, regex):
    match = regex.search(ping_output)
    if not match:
        raise Exception('Invalid PING output:\n' + ping_output)
    return match.groups()
