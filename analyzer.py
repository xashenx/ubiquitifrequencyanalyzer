import time
import logging
import logging.config
import os
import configparser
import threading
import time
import paramiko
from paramiko.ssh_exception import AuthenticationException
import argparse
import socket
import sys
import util.colors as colors
from util.ping import *
import json
from datetime import datetime

# Configuration of logging
CONF_PATH = os.path.dirname(os.path.realpath(__file__)) + '/conf'
logging.config.fileConfig(CONF_PATH + '/log.conf')
# logging.config.fileConfig('/home/ashen/ubiquityfrequencyanalyzer/conf/log.conf')
logger = logging.getLogger(__name__)

# checking if the configuration file is present
config = ''
if not os.path.exists(CONF_PATH + '/analyzer.conf'):
    logger.critical(colors.CRITICAL + 'Configuration file does not exist! trying to create it for you ...' + colors.ENDC)
    try:
        shutil.copy(CONF_PATH + 'analyzer.conf.example', CONF_PATH + 'analyzer.conf')
    except:
        logger.critical(colors.CRITICAL + 'Unable to create analyzer.conf from dummy file'+ colors.ENDC, exc_info=True)
        exit(-1)
    logger.warning(colors.SUCCESS + 'analyzer.conf created from dummy file' + colors.ENDC)
    logger.warning('Please, ' + colors.WARNING + 'fill analyzer.conf' + colors.ENDC + ' with the required information!')
    exit(-1)
else:
    # Initialize configurations
    try:
        config = configparser.ConfigParser()
        config.read(CONF_PATH + '/analyzer.conf')
    except Exception as e:
        logger.error('Failed to read configuration file!', exc_info=True)
        exit(-1)
        
ubnt_user = config.get('login', 'ubnt_user')
ubnt_password = config.get('login', 'ubnt_password')

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        if message == "IP":
            message = "IP error:"
            message += "\n[analyzer.py -a <access_point_ip> -c <client_ip>]"
            message += "\n\nE.G.: analyzer.py -a 172.24.9.3 -c 172.24.9.36"
            message += "\n\n"
            sys.stderr.write(message)
        elif message == "FREQUENCY RANGE":
            message = "Error in the frequency range:"
            message += "\n[analyzer.py -a <access_point_ip> -c <client_ip> -s <start_frequency> -e <end_frequency>]"
            message += "\n\nE.G.: analyzer.py -a 172.24.9.3 -c 172.24.9.36 -s 5500 -e 5700"
            message += "\nNote: frequencies should be between 5500 and 5700 AND <start_frequency> < <end_frequency>"
            message += "\n\n"
            sys.stderr.write(message)
        else:
            self.print_help()
        sys.exit(2)

def is_valid_ipv4_address(address):
    try:
        socket.inet_pton(socket.AF_INET, address)
    except AttributeError:  # no inet_pton here, sorry
        try:
            socket.inet_aton(address)
        except socket.error:
            return False
        return address.count('.') == 3
    except socket.error:  # not a valid address
        return False

    return True

# Opens the connection to the device
# user, password: credentials to login into the device
# RETURN: SSHClient object
def connect(ip):
    # set the tries counter and the connected flag
    connected = False
    tries = 0
    result = 0
    sshcli = None
    # configure the connection variables
    logger.debug('Connecting to ' + ip)
    
    while not connected and tries < 3:
        tries += 1
        try:
            ## open an ssh connection to the device
            sshcli = paramiko.client.SSHClient()
            sshcli.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            sshcli.load_system_host_keys()
            logger.debug('opening connection on {} as {}:{}'.format(ip, ubnt_user, ubnt_password))
            sshcli.connect(ip, port=22,username=ubnt_user, password=ubnt_password, timeout=10)
            connected = True
            result = 0
        except AuthenticationException:
            logger.warning("{}: authetication error (#{})".format(ip, tries))
            result = 1
        except:
            logger.warning("{}: error opening ssh connection (#{})".format(ip, tries), exc_info=True)
            result = 2
        
    # check the result
    if result in (1,2):
        if result == 1:
            # authentication error
            logger.error("Authetication error!")
            connected = False
        else:
            logger.error("Error opening ssh connection!", exc_info=True)
            connected = False
        exit(-1)

    return connected, sshcli

# Closes the connection to the device
# RETURN: result of the execution
def disconnect(sshcli):
    try:
        sshcli.close()
    except:
        logger.error("Error closing ssh connection!")
        return False
    return True

# Sends a command to the device
# RETURNS the output
def send_command(sshcli, command):
    try:
        stdin, stdout, stderr = sshcli.exec_command(command, timeout=10)
    except:
        log.critical('Failed to start the send command to the device!', exc_info=True)
        return False
    return stdout
##
## Checks the status of the device
##
def ping(ip):
    received_rate = expected_received_rate = 0
    try:
        ping_count = 5
        expected_received_rate = 0.8
        ping_result = fping_device(ip, ping_count)
        st = False
        if ping_result['received'] == '0':
            logger.debug("RECEIVED = 0!")
        else:
            received_rate = float(ping_result['received']) / float(ping_count)
            if received_rate >= expected_received_rate:
                st = True
    except:
        logger.error("Error getting status!", exc_info=True)
    return st, ping_result

def parse_device_info(data):
    device_data = str(data).replace('\\n', '')
    device_data = device_data.replace("\\t", "")
    device_data = device_data.replace("\\r", "")
    device_data = device_data.replace('\\', '')
    device_data = device_data[2:len(device_data) - 1]
    device_data = json.loads(device_data)
    frequency = str(float(float(device_data['interfaces'][2]['wireless']['frequency']) / 1000))
    
    # creating a fast access to usefull data!
    device_data['ufa'] = {}
    device_data['ufa']['hostname'] = device_data['host']['hostname']
    device_data['ufa']['essid'] = device_data['interfaces'][2]['wireless']['essid']
    device_data['ufa']['mode'] = device_data['interfaces'][2]['wireless']['mode']
    device_data['ufa']['signal'] = device_data['interfaces'][2]['wireless']['signal']
    device_data['ufa']['noisef'] = device_data['interfaces'][2]['wireless']['noisef']
    device_data['ufa']['ccq'] = float(device_data['interfaces'][2]['wireless']['ccq']) / 10
    device_data['ufa']['txrate'] = device_data['interfaces'][2]['wireless']['txrate']
    device_data['ufa']['rxrate'] = device_data['interfaces'][2]['wireless']['rxrate']
    device_data['ufa']['dfs_locked'] = device_data['board']['radio'][0]['dfs_locked']
    device_data['ufa']['frequency'] = frequency
    if device_data['ufa']['mode'] == 2: # the device is an AP
        device_data['ufa']['clients'] = []
        for client in device_data['connections']['wireless']['sta']:
            device_data['ufa']['clients'].append(client['name'])
    logger.debug(json.dumps(device_data['ufa'], indent=4))
    
    return device_data

## MAIN PROGRAM
def main(ap, client, start=5500, end=5700):
    analysis_dict = {}
    # 1) check if AP and CLIENT are online
    ap_result = ping(ap)
    logger.info("AP is {}".format("ONLINE" if ap_result[0] else "OFFLINE"))
    client_result = ping(client)
    logger.info("CLIENT is {}".format("ONLINE" if client_result[0] else "OFFLINE"))
    if not (ap_result[0] and client_result[0]):
        logger.warning(colors.CRITICAL + 'AP and CLIENT should be both online when starting the test!' + colors.ENDC)
        exit(-1)
        
    # 2) retrieve device data
    # 2.1) connect to AP
    result, sshcli = connect(ap)
    
    result = False
    retries = 3
    while retries and not result:
        result = send_command(sshcli, 'ubntbox status')
        retries -= 1
        if result:
            ap_data = parse_device_info(result.read())
            if ap_data['ufa']['mode'] != 2:
                logger.warning(colors.CRITICAL + 'The device selected as AP is instead a client!' + colors.ENDC)
                exit(-1)
    logger.info('AP hostname is {} and is adverting {} on {}GHz'.format(ap_data['ufa']['hostname'], ap_data['ufa']['essid'], ap_data['ufa']['frequency']))
    disconnect(sshcli)
    
    # 2.2) connect to CLIENT
    result, sshcli = connect(client)
    
    result = False
    retries = 3
    while retries and not result:
        result = send_command(sshcli, 'ubntbox status')
        retries -= 1
        if result:
            client_data = parse_device_info(result.read())
            if ap_data['ufa']['essid'] != client_data['ufa']['essid']:
                logger.warning('the CLIENT is connected to {}, while AP is adverting for {}'.format(client_data['ufa']['essid'], ap_data['ufa']['essid']))
                logger.warning('you MUST choose a CLIENT connected to the AP!')
                exit(-1)
            analysis_dict['original'] = {}
            analysis_dict['original']['frequency'] = client_data['ufa']['frequency']
            analysis_dict['original']['ccq'] = client_data['ufa']['ccq']
            analysis_dict['original']['signal'] = client_data['ufa']['signal']
            analysis_dict['original']['noisef'] = client_data['ufa']['noisef']
            analysis_dict['original']['txrate'] = client_data['ufa']['txrate']
            analysis_dict['original']['rxrate'] = client_data['ufa']['rxrate']
            analysis_dict['original']['dfs_locked'] = False
    disconnect(sshcli)
    
    # 3) cycle over the frequency test to perform tests
    for frequency in range(start, end + 5, 5):
        frequency_str = str(frequency)
        new_frequency = str(frequency).rstrip('0')
        new_frequency = '{}.{}'.format(new_frequency[:1], new_frequency[1:])
        logger.info('performing test on {}GHz'.format(new_frequency))
        
        # 3.1) change the frequency on the AP
        # result, sshcli = connect(ap)
        # result = send_command(sshcli, 'iwconfig ath0')
        # disconnect(sshcli)
        
        # 3.2) wait for the DFS wait time
        # wait time = 60 * 10 + 60
        # if frequency + 30 < 5600:
        #     wait_time = 60 * 5 + 60
        wait_time = 5
        time.sleep(wait_time) # tweak on the frequency delay!
        
        # 3.3) check if the AP is dfs_locked
        result, sshcli = connect(ap)
        
        result = False
        retries = 3
        while retries and not result:
            result = send_command(sshcli, 'ubntbox status')
            retries -= 1
            if result:
                ap_data = parse_device_info(result.read())
                logger.info(colors.WARNING + '{} => {}'.format(frequency, frequency == 5605) + colors.ENDC)
        disconnect(sshcli)
        
        if ap_data['ufa']['dfs_locked']:
            logger.warning(colors.CRITICAL + 'The AP is DFS locked.. this frequency is not to be used' + colors.ENDC)
            analysis_dict[frequency_str] = {}
            analysis_dict[frequency_str]['frequency'] = new_frequency
            analysis_dict[frequency_str]['ccq'] = 0
            analysis_dict[frequency_str]['signal'] = 0
            analysis_dict[frequency_str]['noisef'] = 0
            analysis_dict[frequency_str]['txrate'] = 0
            analysis_dict[frequency_str]['rxrate'] = 0
            analysis_dict[frequency_str]['dfs_locked'] = True
            continue
        
        # 3.4) check if the CLIENT is connected
        retries = 3
        connected = False
        while retries and not connected:
            client_result = ping(client)
            if client_result[0]:
                connected = True
            else:
                time.sleep(20)
            retries -= 1
            
            
        if connected:
            # 3.4a) retrieve link data
            result, sshcli = connect(client)
            
            result = False
            retries = 3
            while retries and not result:
                result = send_command(sshcli, 'ubntbox status')
                retries -= 1
                if result:
                    client_data = parse_device_info(result.read())
                    analysis_dict[frequency_str] = {}
                    analysis_dict[frequency_str]['frequency'] = new_frequency
                    analysis_dict[frequency_str]['ccq'] = client_data['ufa']['ccq']
                    analysis_dict[frequency_str]['signal'] = client_data['ufa']['signal']
                    analysis_dict[frequency_str]['noisef'] = client_data['ufa']['noisef']
                    analysis_dict[frequency_str]['txrate'] = client_data['ufa']['txrate']
                    analysis_dict[frequency_str]['rxrate'] = client_data['ufa']['rxrate']
                    analysis_dict[frequency_str]['dfs_locked'] = False
            disconnect(sshcli)
        else:
            # 3.4b) the client is not connected! someone should check!
            logger.warning(colors.WARNING + 'The client is not connecting to wifi! Someone should check!' + colors.ENDC)
            exit(-1)
        #3.4
        # disconnect(sshcli)
    
    # X) revert the frequency to the original one!
    # result = send_command(sshcli, 'iwconfig ath0 freq {}'.format(ap_frequency))
    # Y) write the result to a logfile
    logger.info(json.dumps(analysis_dict, indent=4))
    with open(datetime.now().strftime("%Y-%m-%d@%H:%M.") + ap_data['ufa']['hostname'] + '.json', 'w') as report_file:
        report_file.write(json.dumps(analysis_dict, indent=4))
    
if __name__ == "__main__":
    parser = MyParser(description='UbiquitiFrequencyAnalyzer')
    parser.add_argument("-a", "--ap", dest="ap", type=str, help="AP ip address")
    parser.add_argument("-c", "--client", dest="client", type=str, help="CLIENT ip address")
    parser.add_argument("-s", "--startfreq", dest="startfreq", type=int, help="start frequency")
    parser.add_argument("-e", "--endfreq", dest="endfreq", type=int, help="end frequency")
    
    args = parser.parse_args()
    if args.ap and args.client:
        # check ap e args
        if is_valid_ipv4_address(args.ap) and is_valid_ipv4_address(args.client) and args.ap != args.client:
            if args.startfreq and args.endfreq:
                if 5700 >= args.startfreq >= 5500 and args.startfreq <= args.endfreq  and not (args.startfreq % 5 and args.endfreq % 5):
                    logger.info('custom range analysis AP on {} and CLIENT on {} [{}MHz-{}MHz]'.format(args.ap, args.client, args.startfreq, args.endfreq))
                    main(args.ap, args.client, start=args.startfreq, end=args.endfreq)
                else:
                    parser.error('FREQUENCY RANGE')
            else:
                logger.info('standard analysis AP on {} and CLIENT on {}'.format(args.ap, args.client))
                main(args.ap, args.client)
        else:
            parser.error('IP')
    else:
        parser.print_help()
        sys.exit(2)
    
    